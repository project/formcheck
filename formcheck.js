// $Id: formcheck.js,v 1.5 2008-12-05 06:42:16 jimmy Exp $

var formcheckIsSubmit = false;

$(document).ready(function(){
  $("input[type=submit]").click(function(){
    formcheckIsSubmit = true;
  });
  if (Drupal.settings['formcheck'] ) {
    window.onbeforeunload = formcheckFormCheck;
  }
});


/**
 * Before leaving a page, checks to see if any form has changed
 */
function formcheckFormCheck(e) { 
  if (!e && window.event) {
    e = window.event;
  }

  // Don't run if submit button was clicked or form hasn't changed or there is no event
  if (!formcheckIsSubmit && e) {
    return Drupal.settings.formcheck_message;
  }
}
