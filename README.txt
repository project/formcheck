/**
 *  Form Check
 *  Update: 2012/02/09
 */
 
 
Summary
-------
Optionally adds javascript check to a forms when user try to leave 
a page without save first. On the (/administer/settings/formcheck), 
you can select which forms to attach this behavior to, if user want 
to leave and without save, and there have a warning message window 
at the same time will be display.


Installation and Settings
-------------------------
TO successful completion of this installation. 

1) Install this module and go to setting page (/admin/settings/formcheck).
2) Enter form id per line which is you want to display.


Author
------
Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)
If you use this module, find it useful, and want to send the author a thank 
or you note, feel free to contact me.

The author can also be contacted for paid customizations of this and other modules.